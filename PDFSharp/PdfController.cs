﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;

namespace PDFSharp
{
    public class PdfController
    {
        private static readonly log4net.ILog _log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public PdfDocument OpenDocument(string nameWithPath)
        {
            try
            {
                return PdfReader.Open(nameWithPath, PdfDocumentOpenMode.Import);

            }
            catch (Exception ex)
            {
                _log.Info(string.Format("Error loading document with name in path '{0}'.", nameWithPath));
                _log.Info(ex.Message);
                return null;
            }
        }

        public PdfDocument SplitDocument(PdfDocument doc, int startPage, int endPage)
        {
            IsDocValid(doc);
            IsStartPageUnderEndPage(startPage, endPage);


        }

        private void IsDocValid(PdfDocument doc)
        {
            if (doc == null)
            {
                throw new ArgumentNullException(doc.FullPath);
            }
        }
        private void IsStartPageUnderEndPage(int start, int end)
        {
            if (start >= end)
            {
                throw new ArgumentOutOfRangeException(string.Format("{0}, {1}",start, end));
            }
        }
    }
}
